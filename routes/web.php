<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// new route
$router->get('/health', function () {
    return array(
        "status" => "ok"
    );
});

$router->get('/', ['middleware' => 'json',function () use ($router) {
    return view('index', ['name' => 'Josepdeveloper']);
}]);

$router->group(['prefix' => 'api/users', 'middleware' => ['json', 'token']], function () use ($router) {
    $router->get('',  ['uses' => 'UsersController@showAllUsers']);
    $router->post('', ['uses' => 'UsersController@create']);
    $router->delete('{id}', ['uses' => 'UsersController@delete']);
    $router->get('{id}', ['uses' => 'UsersController@view']);
    
});
$router->group(['prefix' => 'api', 'middleware' => ['json']], function () use ($router) {
    $router->post('login', ['uses' => 'UsersController@login']);

});
