# Project api with lumen / laravel

Microservice api with lumen php

# Start Project

1)Requeriments
- php >= 7.0
- mysql

2) Download repository
> git clone https://gitlab.com/josephdeveloper/lumen-php-project.git
> cd lumen-php-project

3) Config DB_DATABASE Mysql

> CREATE DATABASE lumen_project;

- DB_CONNECTION=mysql
- DB_HOST=127.0.0.1
- DB_PORT=3306
- DB_DATABASE=lumen_project
- DB_USERNAME=root
- DB_PASSWORD=''

4) Migrations
> php artisan migrate

5) Run Project
> php -S localhost:8000 -t public

6) Access User test

- email: test@test.com
- pw: test@2019

7) Documentation

> POST | http://localhost:8000/api/login

> GET | http://localhost:8000/api/users

> POST | http://localhost:8000/api/users

> GET | http://localhost:8000/api/users/id

> DELETE | http://localhost:8000/api/users/id
