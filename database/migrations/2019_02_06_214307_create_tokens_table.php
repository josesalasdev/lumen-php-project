<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_user');
            $table->string('token');
            $table->timestamps();
        });
        DB::table('tokens')->insert(
            array(
                'id_user' => 1,
                'token' => '5fff06927dc485310262903cd346c1e6fd1c88852f4613bc15bcc50355a293cac0e6f792761858200156a238374f7e49ffaa952949a0686ed9595ff4887e8482',
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tokens');
    }
}
