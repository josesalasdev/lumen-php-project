<?php
namespace App\Http\Middleware;
use App\Tokens;

use Closure;
use Illuminate\Http\Request;

class AuthToken
{
    public function handle(Request $request, Closure $next)
    {
        $token = $request->header('Authorization');
        if ($token) {
            if (Tokens::where('token', $token)->first() != Null){
                return $next($request);
            }
        }
        return response()->json(array(
            "error" => "Unauthorized",
        ), 401);
        
    }
}