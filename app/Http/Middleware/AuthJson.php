<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Http\Request;

class AuthJson
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->isJson()) {
            return $next($request);
        }
        return response()->json(array(
            "error" => "Request should have header 'Accept' with the value: 'application/json'",
        ), 403);
        
    }
}