<?php

namespace App\Http\Controllers;

use App\Tokens;

use Illuminate\Http\Request;
use PharIo\Manifest\Email;

class TokenController extends Controller
{

    public function is_token(Request $request)
    {   
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        $access = Users::where($request->all())->first();
        if($access != Null){
            return response()->json($access, 200);
        }
        return response()->json(array(
            "error" => "Error in user or password",
        ), 401);
    }
}