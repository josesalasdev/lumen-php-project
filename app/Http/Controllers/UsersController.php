<?php

namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;
use PharIo\Manifest\Email;

class UsersController extends Controller
{

    public function create(Request $request)
    {
    
        $token = hash("sha512",md5('wordUltraSecret'));

        $this->validate($request, [
            'first_name' => 'required',
            'email' => 'required|email|unique:users',
            'last_name' => 'required',
            'password' => 'required',
        ]);
        $data = $request->all();
        $data['token'] = $token;

        $user = Users::create($data);
        
        return response()->json($data, 201);
    }

    public function showAllUsers()
    {
        return response()->json(Users::all());
    }

    public function login(Request $request)
    {   
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        $access = Users::where($request->all())->first();
        if($access != Null){
            return response()->json($access, 200);
        }
        return response()->json(array(
            "error" => "Error in user or password",
        ), 401);
    }

    public function delete($id)
    {
        Users::findOrFail($id)->delete();
        return response(Null, 204);
    }
    public function view($id)
    {
        return response()->json(Users::find($id));
    }
}