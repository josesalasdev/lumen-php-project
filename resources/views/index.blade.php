<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <title>Project Template</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body class="container">
    <h4>Project Lumen Api | {{$name}}</h4>
    <div class="row">
        <div class="col s12 teal lighten-4">
            <p> Resource | <a target="_blank" href='https://gitlab.com/josephdeveloper/lumen-php-project.git'>https://gitlab.com/josephdeveloper/lumen-php-project.git
                </a></p>
        </div>
    </div>
    <div class="row">
        <h5>Documentation</h5>
        
    </div>

</body>

</html>